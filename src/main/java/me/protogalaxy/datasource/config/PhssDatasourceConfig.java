package me.protogalaxy.datasource.config;


import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@EnableJpaRepositories("me.protogalaxy.datasource.entity.repository")
public class PhssDatasourceConfig {
}
