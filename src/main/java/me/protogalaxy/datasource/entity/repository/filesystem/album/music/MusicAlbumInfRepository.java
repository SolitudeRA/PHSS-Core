package me.protogalaxy.datasource.entity.repository.filesystem.album.music;

import me.protogalaxy.datasource.entity.core.filesystem.album.music.MusicAlbumInfEntity;
import org.springframework.data.repository.CrudRepository;

public interface MusicAlbumInfRepository extends CrudRepository<MusicAlbumInfEntity, Integer> {
}
