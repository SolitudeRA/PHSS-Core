package me.protogalaxy.datasource.entity.repository.filesystem.album.music;

import me.protogalaxy.datasource.entity.core.filesystem.album.music.MusicAlbumInfStaticEntity;
import org.springframework.data.repository.CrudRepository;

public interface MusicAlbumInfStaticRepository extends CrudRepository<MusicAlbumInfStaticEntity, Integer> {
}
