package me.protogalaxy.datasource.entity.repository.filesystem.album.music;

import me.protogalaxy.datasource.entity.core.filesystem.album.music.MusicTrackInfStaticEntity;
import org.springframework.data.repository.CrudRepository;

public interface MusicTrackInfStaticRepository extends CrudRepository<MusicTrackInfStaticEntity, Integer> {
}
