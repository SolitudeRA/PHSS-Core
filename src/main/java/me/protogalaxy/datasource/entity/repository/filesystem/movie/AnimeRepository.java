package me.protogalaxy.datasource.entity.repository.filesystem.movie;

import me.protogalaxy.datasource.entity.core.filesystem.movie.AnimeEntity;
import org.springframework.data.repository.Repository;

public interface AnimeRepository extends Repository<AnimeEntity,Integer> {
}
