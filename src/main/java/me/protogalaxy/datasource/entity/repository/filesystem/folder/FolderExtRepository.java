package me.protogalaxy.datasource.entity.repository.filesystem.folder;

import me.protogalaxy.datasource.entity.core.filesystem.folder.FolderExtEntity;
import org.springframework.data.repository.CrudRepository;

public interface FolderExtRepository extends CrudRepository<FolderExtEntity, Integer> {
}
