package me.protogalaxy.datasource.entity.repository.filesystem.illustration;

import me.protogalaxy.datasource.entity.core.filesystem.illustration.IllustrationEntity;
import org.springframework.data.repository.Repository;

public interface IllustrationRepository extends Repository<IllustrationEntity, Integer> {
}
