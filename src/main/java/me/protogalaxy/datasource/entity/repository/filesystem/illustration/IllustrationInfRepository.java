package me.protogalaxy.datasource.entity.repository.filesystem.illustration;

import me.protogalaxy.datasource.entity.core.filesystem.illustration.IllustrationInfEntity;
import org.springframework.data.repository.CrudRepository;

public interface IllustrationInfRepository extends CrudRepository<IllustrationInfEntity, Integer> {
}
