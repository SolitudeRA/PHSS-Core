package me.protogalaxy.datasource.entity.repository.filesystem.album.music;

import me.protogalaxy.datasource.entity.core.filesystem.album.music.MusicTrackInfEntity;
import org.springframework.data.repository.CrudRepository;

public interface MusicTrackInfRepository extends CrudRepository<MusicTrackInfEntity, Integer> {
}
