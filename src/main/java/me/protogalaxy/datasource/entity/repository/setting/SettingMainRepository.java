package me.protogalaxy.datasource.entity.repository.setting;

import me.protogalaxy.datasource.entity.core.setting.SettingMainEntity;
import org.springframework.data.repository.CrudRepository;

public interface SettingMainRepository extends CrudRepository<SettingMainEntity, Integer> {
}
