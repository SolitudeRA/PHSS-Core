package me.protogalaxy.datasource.entity.repository.filesystem.illustration;

import me.protogalaxy.datasource.entity.core.filesystem.illustration.IllustrationInfGenreEntity;
import org.springframework.data.repository.CrudRepository;

public interface IllustrationInfGenreRepository extends CrudRepository<IllustrationInfGenreEntity, Integer> {
}
