package me.protogalaxy.datasource.entity.repository.filesystem.movie;

import me.protogalaxy.datasource.entity.core.filesystem.movie.AnimeInfEntity;
import org.springframework.data.repository.CrudRepository;

public interface AnimeInfRepository extends CrudRepository<AnimeInfEntity, Integer> {
}
