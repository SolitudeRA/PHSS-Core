package me.protogalaxy.datasource.entity.repository.personaldata.calender;

import me.protogalaxy.datasource.entity.core.personaldata.calender.CalenderEventEntity;
import org.springframework.data.repository.CrudRepository;

public interface CalenderEventRepository extends CrudRepository<CalenderEventEntity, Integer> {
}
