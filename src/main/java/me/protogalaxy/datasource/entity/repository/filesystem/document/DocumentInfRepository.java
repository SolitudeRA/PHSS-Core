package me.protogalaxy.datasource.entity.repository.filesystem.document;

import me.protogalaxy.datasource.entity.core.filesystem.document.DocumentInfEntity;
import org.springframework.data.repository.CrudRepository;

public interface DocumentInfRepository extends CrudRepository<DocumentInfEntity, Integer> {
}
